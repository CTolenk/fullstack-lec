const databaseConfig = {
    host: 'localhost',
    port: 5432,
    username: 'carlostolentino',
    password: '123456',
    database: 'lec-events',
    logging: false,
    synchronize: false
};

if (process.env.NODE_ENV === 'testing') {
    databaseConfig.database = 'lec-events-test';
    databaseConfig.synchronize = true;
}

export default databaseConfig;
