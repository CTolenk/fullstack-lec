const { LiveReloadCompiler } = require('@nestjs/ng-universal');

const compiler = new LiveReloadCompiler({
    projectName: 'lec-events',
});
compiler.run();
