![Maintenance](https://img.shields.io/maintenance/yes/2019?style=for-the-badge)
![Snyk Vulnerabilities for GitHub Repo](https://img.shields.io/snyk/vulnerabilities/github/adrien2p/teanjs?style=for-the-badge)
![GitHub issues](https://img.shields.io/github/issues/adrien2p/teanjs?style=for-the-badge)
![GitHub top language](https://img.shields.io/github/languages/top/adrien2p/teanjs?style=for-the-badge)
![GitHub repo size](https://img.shields.io/github/repo-size/adrien2p/teanjs?style=for-the-badge)

# LEC Events Fullstack

Plataforma para mostrar todos los eventos de la República Dominicana y España

